#!/usr/bin/ruby

require 'pg'

begin
#Do not store password in versioned code!
    con = PG.connect :dbname => 'testdb', :user => '---', 
        :password => '---'

    user = con.user
    db_name = con.db
    pswd = con.pass
    
    puts "User: #{user}"
    puts "Database name: #{db_name}"
    puts "Password: #{pswd}" 
    
rescue PG::Error => e

    puts e.message 
    
ensure

    con.close if con
    
end
